# [RequestHub.io](https://requesthub.io "RequestHub.io url")

**Voting for GitHub!** RequestHub is a simple tool that uses the GitHub API to allow people to vote on features/issues for their favorite projects.

While GitHub is an amazing application that adds a "missing" layer to what `git` provides out of the box, it's scope is still limited to solving a specific problem. Many developers have the need for a "feedback layer" running on top of the GitHub application. So, [RequestHub.io](https://requesthub.io "RequestHub.io url") provides a simple feedback mechanism without the owner of a repo having to do anything new.

In fact, [RequestHub.io](https://requesthub.io "RequestHub.io url") works regardless of whether the owner of the repo in question uses it themselves.

## How to use

RequestHub works with your existing repositories, so you don't have to change anything.

**Step 1.** Go to the project you're interested in on GitHub.

![step 1](https://raw.githubusercontent.com/asimpletune/RequestHub.io/master/public/images/step_1.png)

**Step 2.** Replace the url of the GitHub project with "RequestHub.io"

![step 2](https://raw.githubusercontent.com/asimpletune/RequestHub.io/master/public/images/step_2.png)

**Step 3.** Vote!
![step 3](https://raw.githubusercontent.com/asimpletune/RequestHub.io/master/public/images/step_3.png)
